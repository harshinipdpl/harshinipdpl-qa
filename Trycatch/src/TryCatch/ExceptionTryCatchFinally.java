	package TryCatch;
		//one try can be followed by multiple catch blocks
		//catch should be an immediate block after try
		//try catch can generally be used for Christmas day special offer popups, next day no offer will return instead of error
		//finally will be executed even if try catch is having error, it will not terminate, it will check for finally and execute the code in finally
		//finally is used to close the browser and delete cookies
		//there was one case where finally cannot be executed: only when JVM is stopped forcefully before execution.
		public class ExceptionTryCatchFinally {

			public static void main(String[] args) {
				// TODO Auto-generated method stub

				int a=7;
				int b=0;
				
				try{
					int k=a/b;
					System.out.println(k);
					
					int arr[] = new int[5];
					System.out.println(arr[7]);
					}
				
				catch(ArithmeticException ae){
					System.out.println("I catched the Arthematic exception error");
				}
				catch(IndexOutOfBoundsException ie){
					System.out.println("I catched the Indexout of bound exception error");
				}
				
				catch(Exception e){
					System.out.println("I catched the error");
				}
				finally
				{
					System.out.println("delete cookies/ close browser");
				}
			}

		


	}


