final class finalDemo {
	//class name is final that means u cannot extend that class
	final void getData(){
		//u cannot overwrite this method again
		
		// final can be used in class level, method level, variable level etc.
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int i=5;
		
		//class name is final that means u cannot extend that class
		
	}

}
