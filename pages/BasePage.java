package com.pages;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;


public class BasePage {
	AppiumDriver driver=null;
	
	public void intializeWebApp() throws MalformedURLException{
		DesiredCapabilities cap=new DesiredCapabilities();
		cap.setCapability(CapabilityType.BROWSER_NAME, "chrome");
		cap.setCapability(CapabilityType.PLATFORM, "Android");
		cap.setCapability("deviceName", "Nexus 5X");
		//cap.setCapability(CapabilityType.VERSION, "7.0");
		 driver=new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		 //driver.get("http://www.myntra.com/");
	}
	public void openMyntra(){
		driver.get("http://www.myntra.com/");
	}
	
	 
	 

@Test
	public void intialisepdm() throws MalformedURLException, InterruptedException{
		
		DesiredCapabilities cap=new DesiredCapabilities();
		cap.setCapability("deviceName", "Nexus 5X");
		//cap.setCapability(CapabilityType.VERSION, "7.0");
		
		cap.setCapability("appPackage", "com.myblue.pdm.debug");
		cap.setCapability("appActivity", "com.myblue.pdm.MainActivity");

		 AppiumDriver driver=new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		 Thread.sleep(10000);	
	}
}
